FROM centos:latest

ARG CLAM_VERSION=0.102.1

RUN echo "exclude=filesystem*" >> /etc/yum.conf

WORKDIR /tmp/clamav-build

COPY ./talos.pub talos.pub

# Install ClamAV
RUN yum update -y -q && \
	yum install -y -q curl-devel gcc-c++ openssl-devel wget make && \
	wget -nv "https://www.clamav.net/downloads/production/clamav-${CLAM_VERSION}.tar.gz" && \
	wget -nv "https://www.clamav.net/downloads/production/clamav-${CLAM_VERSION}.tar.gz.sig" &&  \
	gpg --import /tmp/clamav-build/talos.pub && \
	gpg --decrypt clamav-${CLAM_VERSION}.tar.gz.sig && \
	tar xzf clamav-${CLAM_VERSION}.tar.gz && \
	cd clamav-${CLAM_VERSION} && \
	./configure && \
	make && \
	make install && \
	rm -rf /tmp/clamav-build && \
	yum remove -y -q wget make gcc-c++ openssl-devel kernel-headers && \
	yum clean all

WORKDIR /

# Add clamav user
RUN groupadd -r clamav && \
	useradd -r -g clamav -u 1000 clamav -d /var/lib/clamav && \
	mkdir -p /var/lib/clamav && \
	mkdir /usr/local/share/clamav && \
	chown -R clamav:clamav /var/lib/clamav /usr/local/share/clamav

# Configure Clam AV...
RUN chown clamav:clamav -R /usr/local/etc/
COPY --chown=clamav:clamav ./*.conf /usr/local/etc/
COPY --chown=clamav:clamav eicar.com /
COPY --chown=clamav:clamav ./readyness.sh /

# Initial update of av databases
# --no-dns is needed to work around the following error:
#   Mirror https://database.clamav.net is not synchronized.
# (https://clamav-users.clamav.narkive.com/TO6a1pMk/cdiff-errors-clamav-db-update-failed-mirror-is-not-synchronized#post4)
RUN freshclam --no-dns && \
	chown clamav:clamav /var/lib/clamav/*.cvd

# Fix permissions
RUN mkdir -p /var/run/clamav && \
	chown clamav:clamav /var/run/clamav && \
	chmod 750 /var/run/clamav

USER 1000

VOLUME /var/lib/clamav

COPY --chown=clamav:clamav docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 3310
